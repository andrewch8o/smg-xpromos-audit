"""Script to scrape x-promos from the thank-you pages of the specified assets"""
import os
import re
import csv
import requests
from tqdm import tqdm
from time import sleep
from bs4 import BeautifulSoup
from argparse import ArgumentParser

def get_token(url):
    '''Extracts an asset token from a url'''
    res = re.search(r'/(?P<token>cw-cp-[^\.]+)\.html', url)
    if res:
        return res.groupdict()['token']
    else:
        return '---'

def get_asset_url(asset_token):
    '''Returns url from an asset token'''
    return 'https://www2.simplermedia.com/'+ asset_token +'-ty.html'

def try_scrape_ty_page(asset):
    '''Scrapes asset ids and URLs from the thank-you page for the given asset.
       returns [(asset_id, url), ...]
       asset_id is None if the URL does not have asset id (for example a
       content article)
       Raises exception if fails to load thank-you page
    '''
    asset_url = get_asset_url(asset)
    page = requests.get(asset_url)
    
    if page.status_code != 200:
        raise Exception(f'Failed to retrieve thank-you page: \'{asset_url}\'')
    
    soup = BeautifulSoup(page.text, 'html.parser')
    page_type = None
    if soup.find('nav', {'role':'navigation'}) is None:
        x_promos = new_ty_page_try_scrape(page_soup=soup)
        page_type = 'new'
    else:
        x_promos = old_ty_page_try_scrape(page_soup=soup)
        page_type = 'old'

    xp_set = set(x_promos)
    result = list()
    for url in xp_set:
        result.append((page_type, get_token(url), url))
    return result

def new_ty_page_try_scrape(page_soup):
    '''Scrapes all 'div' elements containing x-promo blocks for 'a' elements
       and scrapes their 'href' into a list
    '''
    xp_divs = page_soup.find('div', {'class':'bg-wrap'}).findAll('div', {'class':'col-lg-12 content-wrap__content-block'})
    x_promos = []
    for d in xp_divs:
        for a in d.findAll('a'):
            x_promos.append(a['href'])
    return x_promos

def old_ty_page_try_scrape(page_soup):
    '''Scrapes 'href' attributes from all 'a' tags BUT
       Skips first 'a' ( logo link )
       Stops when encounters '/white-papers/' in a link ( 'Visit our resource library' link under x-promos )
    '''
    a_list = page_soup.findAll('a')
    
    x_promos = []
    for a in a_list[1:]:
        a_href = a['href']
        if '/white-papers/' in a_href:
            break
        x_promos.append(a_href)
    return x_promos

def scrape_xpromos(asset_ids):
    '''Scrapes x-promos from all the assets'''
    xmappings = {}
    for id in tqdm(set(asset_ids)):
        try:
            xmappings[id] = try_scrape_ty_page(id)
            sleep(2)
        except Exception as e:
            xmappings[id] = [('---', str(e))]
    return xmappings

def get_program_id(asset_id):
    '''Converts asset id into program id 
       by removing the '-\d\d' at the end of asset id
       Example 'cw-cp-contentserv-cxm-2019-01' -> 'cw-cp-contentserv-cxm-2019' 
       Example 'cw-cp-contentserv-cxm-2019-02' -> 'cw-cp-contentserv-cxm-2019' 
    '''
    return re.sub(r'-\d\d$', '', asset_id)
    

if __name__ == "__main__":
    parser = ArgumentParser(description='Utility to scrape x-promos from CMSWire Thank-You pages')
    parser.add_argument('-i', dest='i_file', required=True, metavar='iFILE',
                        help='Input file with the list of asset ids to scan')
    parser.add_argument('-o', dest='o_file', required=True, metavar='oFILE',
                        help='Output CSV file to save the scraped data')
    args = parser.parse_args()

    i_file_path = os.path.abspath(args.i_file)
    scraped_data = None
    try:
        input = open(i_file_path)
    except IOError:
        print(f'Failed to find input file at \'{i_file_path}\'')
        exit
    with input:
        input_clean = map(str.strip, input.readlines())
        scraped_data = scrape_xpromos(input_clean)

    o_file_path = os.path.abspath(args.o_file)
    try:
        output = open(o_file_path, mode='w+', newline='')
    except IOError:
        print(f'Failed to open output file at \'{o_file_path}\'')
        exit
    with output:
        csv_writer = csv.writer(output, dialect='excel')
        csv_writer.writerow(['program','asset','ty_page','promo_asset','promo_url'])
        for key in scraped_data:
            program_id = get_program_id(key)
            scraped_xpromos = scraped_data[key]
            for entry in scraped_xpromos:
                data_row = list(entry)
                data_row.insert(0, key)
                data_row.insert(0, program_id)
                csv_writer.writerow(data_row)
    print(f'Done. Data saved to \'{o_file_path}\'')    