The project automates the process of verifying that all the cross-promos are valid (promote active campaigns).

Approach:

* Scrape cross-promo data from the active campaign landing pages
* Cross-checks scraped data with the list of active campaigns to identify the cross-promos that need to be replaced 

Usage:

1. Fetch the list of active assets into a file ( ../MON/DAY/assets.active.txt)
2. Run cleanup-asset-list.py on the aforementioned file ( cleans up the list, removes unused entries)
    >> creates clean.assets.active.txt
3. Run scrape-x-promos.py 
    >> input - clean.assets.active.txt
    >> output - scraped x promos csv ( asset-id / x-promo-id / x-promo-url)
4. IMport scraped x-promos csv into a Google Sheet
5. Save x-promo-id column into a file ( list of assets being x-promoed)
6. Run the list-inactive-promoted-assets.py
    >> input - x-promo-id s file
    >> input - clean.assets.active.txt
    >> output - list of x-promoed assets which are not in the active asset list