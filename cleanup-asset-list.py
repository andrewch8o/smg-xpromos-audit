"""This script is intended to consume list of assets from a file,
clean up the list and write the cleaned data to new file 
next to the original one"""
import os
from argparse import ArgumentParser

if __name__ == "__main__":
    parser = ArgumentParser(description='Utility to clean up the list of active assets from a file')
    parser.add_argument('-i', dest='i_file', required=True, metavar='iFILE',
                        help='Input file with the list of asset ids to process')
    args = parser.parse_args()

    i_file_path = os.path.abspath(args.i_file)
    input_data = None
    try:
        input = open(i_file_path)
    except IOError:
        print(f'Failed to find input file at \'{i_file_path}\'')
        exit
    with input:
        input_data = input.readlines()

    clean_data = []
    for asset_id in input_data:
        asset_id_clean = asset_id.replace('"','')
        if any(x in asset_id_clean for x in ['-ls-','-ms-']):
            continue # skip any late- mid-stage assets
        if not asset_id_clean.strip():
            continue # skip if string is empty
        clean_data.append(asset_id_clean)

    path, fname = os.path.split(i_file_path)
    o_file_path = os.path.join(path, 'clean.'+fname)
    try:
        output = open(o_file_path, mode='w+')
    except IOError:
        print(f'Failed to open output file at \'{o_file_path}\'')
        exit
    with output:
        output.writelines(clean_data)
    print(f"Done. Cleaned data saved to '{o_file_path}'")