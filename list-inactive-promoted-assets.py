"""The script compares two input lists ( 'active assets' and 'promoted assets' )
from two input files ( each entry on a new line in each fiel ). Prints those 
entries that are in the 'promoted assets' but are NOT in 'active assets' """
import os
from argparse import ArgumentParser

def strip_and_filter_empty(ilist):
    '''For a given list strips entries and filters out empty entries'''
    return  [x for x in map(str.strip, ilist) if x!= '']

if __name__ == "__main__":
    parser = ArgumentParser(description='Utility to clean up the list of active assets from a file')
    parser.add_argument('-iact', dest='iactive', required=True, metavar='iACTIVE_ASSETS_FILE',
                        help='Input file with the list of asset ids for the currently active assets')
    parser.add_argument('-iprom', dest='ipromoted', required=True, metavar='iPROMOTED_ASSETS_FILE',
                        help='Input file with the list of asset ids for the currently promoted assets')
    args = parser.parse_args()

    iact_path = os.path.abspath(args.iactive)
    iprom_path = os.path.abspath(args.ipromoted)

    #loading active assets
    active = []
    with open(iact_path, 'r') as f:
        active = f.readlines()
    active = strip_and_filter_empty(active)
    
    #loading promoted assets            
    promoted = []
    with open(iprom_path, 'r') as f:
        promoted = f.readlines()
    promoted = strip_and_filter_empty(promoted)            
    
    diff = set(promoted) - set(active)
    print("Promoted but not active:")
    for i in diff:
        print(i)